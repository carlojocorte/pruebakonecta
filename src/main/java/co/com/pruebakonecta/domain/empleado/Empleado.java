package co.com.pruebakonecta.domain.empleado;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad de Empleado
 * @author Carlos Cortés
 *
 */
@Entity
@Table(name = "Empleado")
public class Empleado implements Serializable{

	private static final long serialVersionUID = 8734662706668205089L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;
	
	@Column(name = "fecha_ingreso")
	private Date fechaIngreso;
	
	@Column (name = "nombre")
	private String nombre;
	
	@Column (name = "salario")
	private int salario;
	
	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the fechaIngreso
	 */
	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	/**
	 * @param fechaIngreso the fechaIngreso to set
	 */
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the salario
	 */
	public int getSalario() {
		return salario;
	}

	/**
	 * @param salario the salario to set
	 */
	public void setSalario(int salario) {
		this.salario = salario;
	}

	public Empleado() {
		super();
	}

	public Empleado(BigInteger id,Date fechaIngreso, String nombre, int salario) {
		this.id = id;
		this.fechaIngreso = fechaIngreso;
		this.nombre = nombre;
		this.salario = salario;
	}
	
	
	
}
