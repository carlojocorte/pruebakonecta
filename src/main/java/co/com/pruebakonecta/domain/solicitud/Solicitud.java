package co.com.pruebakonecta.domain.solicitud;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import co.com.pruebakonecta.domain.empleado.Empleado;

/**
 * Entidad de Solicitud
 * @author Carlos Cortés
 *
 */
@Entity
@Table(name = "Solicitud")
public class Solicitud implements Serializable {
	

	private static final long serialVersionUID = -8538496959817405572L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public BigInteger id;

	@Column(name = "codigo")
	private String codigo;
	
	@Column (name = "descripcion")
	private String descripcion;
	
	@Column (name = "resumen")
	private String resumen;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empleado_id")
	private Empleado empleado;

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the resumen
	 */
	public String getResumen() {
		return resumen;
	}

	/**
	 * @param resumen the resumen to set
	 */
	public void setResumen(String resumen) {
		this.resumen = resumen;
	}

	/**
	 * @return the empleado
	 */
	public Empleado getEmpleado() {
		return empleado;
	}

	/**
	 * @param empleado the empleado to set
	 */
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
	
	

	
}
