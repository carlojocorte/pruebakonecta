package co.com.pruebakonecta.service.solicitud;

import java.math.BigInteger;
import java.util.Optional;

import co.com.pruebakonecta.shared.dto.SolicitudDTO;
import co.com.pruebakonecta.shared.repsonse.SolicitudResponse;

/**
 * Se define la interfaz de los métodos de la capa de servicio para la entidad solicitud
 * @author Carlos Cortés 
 *
 */
public interface SolicitudService {
	
	Optional<SolicitudResponse> findById(BigInteger id);
	
	Optional<SolicitudResponse> findByCodigo(String codigo);
	
	SolicitudDTO saveNewSolicitud(SolicitudDTO solicitudDTO);

}
