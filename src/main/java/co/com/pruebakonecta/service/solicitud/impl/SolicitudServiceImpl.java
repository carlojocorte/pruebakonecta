package co.com.pruebakonecta.service.solicitud.impl;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import co.com.pruebakonecta.domain.solicitud.Solicitud;
import co.com.pruebakonecta.repository.solicitud.SolicitudRepository;
import co.com.pruebakonecta.service.solicitud.SolicitudService;
import co.com.pruebakonecta.shared.dto.SolicitudDTO;
import co.com.pruebakonecta.shared.repsonse.SolicitudResponse;

/**
 * Se definen las implementaciones de la interfaz SolicitudService
 * @author Carlos Cortés 
 *
 */
@Service
public class SolicitudServiceImpl implements SolicitudService {
	
	private final SolicitudRepository solicitudRepository;
	
	public SolicitudServiceImpl(SolicitudRepository solicitudRepository) {
		this.solicitudRepository = solicitudRepository;
	}

	/**
	 * Implementación de método para encontrar solicitud por Id
	 */
	@Override
	public Optional<SolicitudResponse> findById(BigInteger id) {
		Optional<Solicitud> solicitudOptional = solicitudRepository.findById(id);
		if (solicitudOptional.isPresent()) {
			Solicitud solicitud = solicitudOptional.get();
			return Optional.of(new SolicitudResponse(solicitud.getCodigo(), solicitud.getDescripcion(), solicitud.getResumen(), 
					solicitud.getEmpleado().getNombre()));
		} else {
			return Optional.empty();
		}
	}

	/**
	 * Implementación de método para encontrar solicitud por código
	 */
	@Override
	public Optional<SolicitudResponse> findByCodigo(String codigo) {
		Optional<Solicitud> solicitudOptional = solicitudRepository.findByCodigo(codigo);
		if (solicitudOptional.isPresent()) {
			Solicitud solicitud = solicitudOptional.get();
			return Optional.of(new SolicitudResponse(solicitud.getCodigo(), solicitud.getDescripcion(), solicitud.getResumen(), 
					solicitud.getEmpleado().getNombre()));
		} else {
			return Optional.empty();
		}
	}

	/**
	 * Implementación de método para crear una nueva solicitud
	 */
	@Override
	public SolicitudDTO saveNewSolicitud(SolicitudDTO solicitudDTO) {
		Solicitud solicitud = new Solicitud();
		BeanUtils.copyProperties(solicitudDTO, solicitud);
		solicitudRepository.save(solicitud);
		solicitudDTO.setId(solicitud.getId());
		return solicitudDTO;
	}

}
