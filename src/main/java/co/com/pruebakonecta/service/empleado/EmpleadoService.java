package co.com.pruebakonecta.service.empleado;

import java.math.BigInteger;
import java.util.Optional;

import co.com.pruebakonecta.shared.dto.EmpleadoDTO;

/**
 * Se define la interfaz de los métodos de la capa de servicio para la entidad empleado
 * @author Carlos Cortés 
 *
 */
public interface EmpleadoService {

	Optional<EmpleadoDTO> findById(BigInteger id);
	
	Optional<EmpleadoDTO> findByNombre(String nombre);
	
	EmpleadoDTO saveNewEmpleado(EmpleadoDTO empleado);

		
}
