package co.com.pruebakonecta.service.empleado.impl;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import co.com.pruebakonecta.domain.empleado.Empleado;
import co.com.pruebakonecta.repository.empleado.EmpleadoRepository;
import co.com.pruebakonecta.service.empleado.EmpleadoService;
import co.com.pruebakonecta.shared.dto.EmpleadoDTO;

/**
 * Se definen las implementaciones de la interfaz EmpleadoService
 * @author Carlos Cortés 
 *
 */
@Service
public class EmpleadoServiceImpl implements EmpleadoService {

	private final EmpleadoRepository empleadoRepository;
	
	public EmpleadoServiceImpl(EmpleadoRepository empleadoRepository) {
		this.empleadoRepository = empleadoRepository;
	}
	
	/**
	 * Implementación de método para encontrar empleado por Id
	 */
	@Override
	public Optional<EmpleadoDTO> findById(BigInteger id) {
		
		Optional<Empleado> empleadoOptional = empleadoRepository.findById(id);
		if (empleadoOptional.isPresent()) {
			Empleado empleado = empleadoOptional.get();
			return Optional.of(new EmpleadoDTO(empleado.getId(), empleado.getFechaIngreso(), empleado.getNombre(), empleado.getSalario()));
		} else {
			return Optional.empty();
		}
		
	}
	
	/**
	 * Implementación de método para encontrar empleado por Nombre
	 */
	@Override
	public Optional<EmpleadoDTO> findByNombre(String nombre) {
		Optional<Empleado> empleadoOptional = empleadoRepository.findByNombre(nombre);
		if (empleadoOptional.isPresent()) {
			Empleado empleado = empleadoOptional.get();
			return Optional.of(new EmpleadoDTO(empleado.getId(), empleado.getFechaIngreso(), empleado.getNombre(), empleado.getSalario()));
		} else {
			return Optional.empty();
		}
	}

	/**
	 * Implementación de método para guardar un nuevo empleado
	 */
	@Override
	public EmpleadoDTO saveNewEmpleado(EmpleadoDTO empleadoDTO) {
		Empleado empleado = new Empleado();
		BeanUtils.copyProperties(empleadoDTO, empleado);
		empleadoRepository.save(empleado);
		empleadoDTO.setId(empleado.getId());
		return empleadoDTO;
	}
}
