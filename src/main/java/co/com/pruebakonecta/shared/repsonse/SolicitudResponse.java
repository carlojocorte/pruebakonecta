package co.com.pruebakonecta.shared.repsonse;

import java.math.BigInteger;

/**
 * Se define el objeto de reppuesta para las solicitudes
 * @author Carlos Cortés 
 *
 */
public class SolicitudResponse {
	
	private BigInteger id;
	private String codigo;
	private String descripcion;
	private String resumen;
	private BigInteger idEmpleado;
	private String nombreEmpleado;

	public SolicitudResponse() {
	}

	public SolicitudResponse(String codigo, String descripcion, String resumen,
			 String nombreEmpleado) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.resumen = resumen;
		this.nombreEmpleado = nombreEmpleado;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the resumen
	 */
	public String getResumen() {
		return resumen;
	}

	/**
	 * @param resumen the resumen to set
	 */
	public void setResumen(String resumen) {
		this.resumen = resumen;
	}

	/**
	 * @return the idEmpleado
	 */
	public BigInteger getIdEmpleado() {
		return idEmpleado;
	}

	/**
	 * @param idEmpleado the idEmpleado to set
	 */
	public void setIdEmpleado(BigInteger idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	/**
	 * @return the nombreEmpleado
	 */
	public String getNombreEmpleado() {
		return nombreEmpleado;
	}

	/**
	 * @param nombreEmpleado the nombreEmpleado to set
	 */
	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}
	
	
	

}
