package co.com.pruebakonecta.shared.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Se define el DTO para la entidad empleado
 * @author Carlos Cortés 
 *
 */
public class EmpleadoDTO implements Serializable {


	private static final long serialVersionUID = -513360373967913169L;

	private BigInteger id;
	private Date fechaIngreso;
	private String nombre;
	private int salario;
	
	
	
	public EmpleadoDTO(BigInteger id,Date fechaIngreso, String nombre, int salario) {
		super();
		this.setId(id);
		this.fechaIngreso = fechaIngreso;
		this.nombre = nombre;
		this.salario = salario;
	}
	
	

	public EmpleadoDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the fechaIngreso
	 */
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	
	/**
	 * @param fechaIngreso the fechaIngreso to set
	 */
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * @return the salario
	 */
	public int getSalario() {
		return salario;
	}
	
	/**
	 * @param salario the salario to set
	 */
	public void setSalario(int salario) {
		this.salario = salario;
	}



	public BigInteger getId() {
		return id;
	}



	public void setId(BigInteger id) {
		this.id = id;
	}
	
}
