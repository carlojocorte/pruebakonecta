package co.com.pruebakonecta.shared.dto;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Se define el DTO para la entidad solicitud
 * @author Carlos Cortés 
 *
 */
public class SolicitudDTO implements Serializable {

	private static final long serialVersionUID = 4680146899462839330L;
	

	private BigInteger id;
	private String codigo;
	private String descripcion;
	private String resumen;
	private BigInteger idEmpleado;
	
	
	
	public SolicitudDTO(String codigo, String descripcion, String resumen, BigInteger idEmpleado) {
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.resumen = resumen;
		this.idEmpleado = idEmpleado;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * @return the resumen
	 */
	public String getResumen() {
		return resumen;
	}
	
	/**
	 * @param resumen the resumen to set
	 */
	public void setResumen(String resumen) {
		this.resumen = resumen;
	}
	
	/**
	 * @return the idEmpleado
	 */
	public BigInteger getIdEmpleado() {
		return idEmpleado;
	}
	
	/**
	 * @param idEmpleado the idEmpleado to set
	 */
	public void setIdEmpleado(BigInteger idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}
	
}
