package co.com.pruebakonecta.controller.solicitud;

import java.math.BigInteger;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pruebakonecta.service.solicitud.SolicitudService;
import co.com.pruebakonecta.shared.dto.SolicitudDTO;
import co.com.pruebakonecta.shared.repsonse.SolicitudResponse;

/**
 * Controlador para consultar y agregar registros a la tabla solicitud
 * @author Carlos Cortés 
 *
 */
@RestController
@RequestMapping("/solicitud")
public class SolicitudController {

	private final SolicitudService solicitudService;
		
	/**
	 * Inyección de la dependencia SolicitudService
	 * @param solicitudService
	 */
	public SolicitudController(SolicitudService solicitudService) {
		this.solicitudService = solicitudService;
	}

	/**
	 * Buscar solicitud por id
	 * @param id
	 * @return
	 */
	@GetMapping("/id/{id}")
	public ResponseEntity<SolicitudResponse> getSolicitudById(@PathVariable BigInteger id ) {
		return solicitudService.findById(id).isPresent() ? ResponseEntity.ok(solicitudService.findById(id).get()) : 
			ResponseEntity.notFound().build();
	}
	
	/**
	 * Buscar solicitud por código
	 * @param nombre
	 * @return
	 */
	@GetMapping("/codigo/{codigo}")
	public ResponseEntity<SolicitudResponse> getEmpleadoByCodigo(@PathVariable String nombre ) {
		return solicitudService.findByCodigo(nombre).isPresent() ? ResponseEntity.ok(solicitudService.findByCodigo(nombre).get()) : 
			ResponseEntity.notFound().build();
	}
	
	/**
	 * Crear una nueva solicitud	
	 * @param solicitudDTO
	 * @return
	 */
	@PostMapping
	public ResponseEntity<SolicitudDTO> saveSolicitud(@RequestBody SolicitudDTO solicitudDTO) {
		return ResponseEntity.ok(solicitudService.saveNewSolicitud(solicitudDTO));
	}
	
}
