package co.com.pruebakonecta.controller.empleado;

import java.math.BigInteger;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pruebakonecta.service.empleado.EmpleadoService;
import co.com.pruebakonecta.shared.dto.EmpleadoDTO;

/**
 * Controller para consultar y agregar registros a la tabla empleado
 * @author Carlos Cortés 
 *
 */
@RestController
@RequestMapping("/empleado")
public class EmpleadoController {
	
	private final EmpleadoService empleadoService;
	
	/**
	 * Inyección de la dependencia SolicitudService
	 * @param empleadoService
	 */
	public EmpleadoController(EmpleadoService empleadoService) {
		this.empleadoService = empleadoService;
	}

	/**
	 * Buscar empleado por id
	 * @param id
	 * @return
	 */
	@GetMapping("/id/{id}")
	public ResponseEntity<EmpleadoDTO> getEmpleadoById(@PathVariable BigInteger id ) {
		return empleadoService.findById(id).isPresent() ? ResponseEntity.ok(empleadoService.findById(id).get()) : 
			ResponseEntity.notFound().build();
	}
	/**
	 * Buscar empleado por el nombre
	 * @param nombre
	 * @return
	 */
	@GetMapping("/nombre/{nombre}")
	public ResponseEntity<EmpleadoDTO> getEmpleadoByNombre(@PathVariable String nombre ) {
		return empleadoService.findByNombre(nombre).isPresent() ? ResponseEntity.ok(empleadoService.findByNombre(nombre).get()) : 
			ResponseEntity.notFound().build();
	}
	
	/**
	 * Crear un nuevo empleado
	 * @param empleadoDTO
	 * @return
	 */
	@PostMapping
	public ResponseEntity<EmpleadoDTO> saveEmpleado(@RequestBody EmpleadoDTO empleadoDTO) {
		return ResponseEntity.ok(empleadoService.saveNewEmpleado(empleadoDTO));
	}
}
