package co.com.pruebakonecta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebakonectaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebakonectaApplication.class, args);
	}

}
