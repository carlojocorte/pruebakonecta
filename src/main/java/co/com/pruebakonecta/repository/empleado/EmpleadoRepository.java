package co.com.pruebakonecta.repository.empleado;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pruebakonecta.domain.empleado.Empleado;

/**
 * Interfaz repositorio de Empleado
 * @author Carlos Cortés
 *
 */
public interface EmpleadoRepository extends JpaRepository<Empleado, BigInteger> {

	Optional<Empleado> findByNombre(String nombre);

}
