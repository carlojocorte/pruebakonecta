package co.com.pruebakonecta.repository.solicitud;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pruebakonecta.domain.solicitud.Solicitud;

/**
 * Interfaz repositorio de Solicitud
 * @author Carlos Cortés
 *
 */
public interface SolicitudRepository extends JpaRepository<Solicitud, BigInteger> {

	Optional<Solicitud> findByCodigo(String codigo);

}
